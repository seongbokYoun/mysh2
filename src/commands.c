/**********************************************************************
 * Copyright (C) Jaewon Choi <jaewon.james.choi@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 *********************************************************************/
#include "commands.h"
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <signal.h>
#include <stdlib.h>

static struct command_entry commands[] =
{
  {
    "pwd",
    do_pwd,
    err_pwd
  },
  {
    "cd",
    do_cd,
    err_cd
  },
  {
    "kill",
    do_kill,
    err_kill
  }
};

struct command_entry* fetch_command(const char* command_name)
{
  if (strcmp(command_name, "pwd") == 0)
  {
    return &commands[0];
  }
  else if (strcmp(command_name, "cd") == 0)
  {
    return &commands[1];
  }
  else if (strcmp(command_name, "kill") == 0)
  {
    return &commands[2];
  }
  return NULL;
}

int do_pwd(int argc, char** argv)
{
  char buf[255];
  if (getcwd(buf, 255) != NULL)
  {
    printf("%s\n",buf);
    return 0;
  }
  return -1;
}

void err_pwd(int err_code)
{
  // TODO: Fill it.
}

int do_cd(int argc, char** argv)
{
  chdir(argv[1]);
  switch (errno)
  {
    case 0:
      return 0;
    case ENOENT:
      return 1;
    case ENOTDIR:
      return 2;
    default:
      return -1;
  }
}

void err_cd(int err_code)
{
  switch (err_code)
  {
    case 1:
      fprintf(stderr, "cd: no such file or directory\n");
      break;
    case 2:
      fprintf(stderr, "cd: not a directory\n");
      break;
    default:
      fprintf(stderr, "cd: unknown errorcode\n");
      break;
  }
}

int do_kill(int argc, char** argv)
{
  int pid = atoi(argv[1]);
  if (kill(pid, 9))
  {
    return 0;
  }
  return 1;
}

void err_kill(int err_code)
{
  fprintf(stderr, "kill: send signal fail\n");
}