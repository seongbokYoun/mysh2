/**********************************************************************
 * Copyright (C) Jaewon Choi <jaewon.james.choi@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 *********************************************************************/
#include "fs.h"
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int does_exefile_executable(const char*);

int does_exefile_exists(const char* path)
{
  char* pathEnv = getenv("PATH");
  char seperator = ':';
  const char* pointer = pathEnv;
  char buffer[255] = {0,};
  char* cursor = buffer;
  while(*pointer != '\0')
  {
    if (*pointer == seperator)
    {
      *cursor = '\0';
      strcat(buffer, "/");
      strncat(buffer, path, strlen(path));
      if (does_exefile_executable(buffer))
      {
        return 1;
      }
      cursor = buffer;
    }
    else
    {
      *cursor = *pointer;
      ++cursor;
    }
    ++pointer;
  }

  does_exefile_executable(path);
}

int does_exefile_executable(const char* path)
{
  struct stat sb;
  if (stat(path, &sb) != 0)
    return 0;

  if (S_ISREG(sb.st_mode))
    return 1;
  
  return 0;
}
