/**********************************************************************
 * Copyright (C) Jaewon Choi <jaewon.james.choi@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 *********************************************************************/
#include "parser.h"
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>

#define DEBUG 0

typedef enum { false, true } bool;

void parse_command(const char* input,
                   int* argc, char*** argv)
{
  char buffer[4096] = { 0, };
  strcpy(buffer, input);
  if (DEBUG) printf("Input string is : %s\n", buffer);
  
  //Trim leading space
  const char* start = buffer;
  while(isspace((unsigned char)*start)) ++start;
  //Trim trailing space
  char* end = buffer + strlen(buffer) - 1;
  while (end > buffer && isspace((unsigned char)*end)) --end;
  end[1] = '\0';
  if (DEBUG) printf("After Trim : %s\n", start);

  *argc = 1;

  bool isInQuote = false;
  bool isSplitting = false;
  const char* pointer = start;
  //Counts substrings
  while (*pointer != '\0')
  {
    switch (*pointer)
    {
      case ' ':
        if (isInQuote || isSplitting)
        {
          break;
        }
        isSplitting = true;
        break;

      case '\"':
        isInQuote = !isInQuote;
      default:
        if (isSplitting)
        {
          isSplitting = false;
          *argc += 1;
        }
        break;
    }
    ++pointer;
  }
  if (DEBUG) printf("Count is %d\n", *argc);
  *argv = (char**)malloc(sizeof(char*) * ((*argc) + 1));
  (*argv)[*argc] = NULL;

  isInQuote = false;
  isSplitting = false;
  pointer = start;
  int substringLength = 0;
  int idx = 0;
  //Allocate substrings
  while (*pointer != '\0')
  {
    switch (*pointer)
    {
      case ' ':
        if (isInQuote || isSplitting)
        {
          if (isInQuote)
          {
            if (DEBUG) printf("SubstringLength++ by : %c\n", *pointer);
            ++substringLength;
          }
          break;
        }
        isSplitting = true;

        if (DEBUG) printf("Allocates %d length\n", substringLength+1);
        (*argv)[idx] = (char*)malloc(sizeof(char) * (substringLength + 1));
        substringLength = 0;
        ++idx;
        break;

      case '\"':
        isInQuote = !isInQuote;
        break;
      default:
        if (isSplitting)
        {
          isSplitting = false;
        }
        if (DEBUG) printf("SubstringLength++ by : %c\n", *pointer);
        ++substringLength;
        break;
    }
    ++pointer;
  }
  (*argv)[idx] = (char*)malloc(sizeof(char) * (substringLength + 1));
  if (DEBUG) printf("Allocates %d length\n", substringLength+1);

  idx = 0;
  pointer = start;
  isInQuote = false;
  isSplitting = false;
  char* temp = ((*argv)[idx]);
  //Copy substrings
  while (*pointer != '\0')
  {
    switch (*pointer)
    {
      case ' ':
        if (isInQuote || isSplitting)
        {
          if (isInQuote)
          {
            *temp = *pointer;
            ++temp;
          }
          break;
        }
        isSplitting = true;

        *temp = '\0';
        if (DEBUG) printf("Substring is %s\n", ((*argv)[idx]));
        ++idx;
        temp = ((*argv)[idx]);
        break;

      case '\"':
        isInQuote = !isInQuote;
        break;
      default:
        if (isSplitting)
        {
          isSplitting = false;
        }
        *temp = *pointer;
        ++temp;
        break;
    }
    ++pointer;
  }
  *temp = '\0';
  if (DEBUG) printf("Substring is %s\n", ((*argv)[idx]));
}
